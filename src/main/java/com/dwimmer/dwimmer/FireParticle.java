/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

/**
 *
 * @author kevingroat
 */
public class FireParticle extends Particle{
   
   public FireParticle(){
      this(0, 0, 0, 0, 0, 0);
   }
   
   public FireParticle(double tx, double ty, double tz, double tdx, double tdy, double tdz){
      this(tx, ty, tz, tdx, tdy, tdz, false, false);
   }
   
   public FireParticle(double tx, double ty, double tz, double tdx, double tdy, double tdz, boolean tRot, boolean collide){
      super(tx, ty, tz, tdx, tdy, tdz, null, tRot, collide);
      sprite = DwimmerEngine.fire.clone();
      sprite.enact("spin");
   }
   
   public FireParticle clone(double tx, double ty, double tz, double tdx, double tdy, double tdz){
      return new FireParticle(tx, ty, tz, tdx, tdy, tdz);
   }
   
   public FireParticle clone(double tx, double ty, double tz, double tdx, double tdy, double tdz, boolean rot, boolean collide){
      return new FireParticle(tx, ty, tz, tdx, tdy, tdz, rot, collide);
   }
}
