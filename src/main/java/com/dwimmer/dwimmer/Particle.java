/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

/**
 *
 * @author kevingroat
 */
public class Particle extends Entity{

   protected String effect;
   protected double rot;
   
   public Particle(String tEffect){
      this(0, 0, 0, 0, 0, 0, tEffect);
   }
   
   public Particle(double tx, double ty, double tz, double tdx, double tdy, double tdz, String tEffect){
      this(tx, ty, tz, tdx, tdy, tdz, tEffect, false, false);
   }
   
   public Particle(double tx, double ty, double tz, double tdx, double tdy, double tdz, String tEffect, boolean tRot, boolean collide){
      removeShadow = true;
      sprite = DwimmerEngine.particles.clone();
      x = tx-sprite.getSpriteWidth()/2;
      y = ty-sprite.getSpriteWidth()/2;
      z = tz;
      dx = tdx;
      dy = tdy;
      dz = tdz;
      effect = tEffect;
      sprite.enact(effect);
      hitBox = new Rectangle(0, 0, 0, 0);
      targetable = false;
      if(tRot){
         rot = Math.random()*Math.PI*2;
      }else{
         rot = 0;
      }
      ignoreCollide = !collide;
   }
   
   @Override
   public void paint(Graphics g, Point p){
      if(rot != 0){
         sprite.drawRot(g, p.x, p.y-(int)z/2, rot);
      }else{
         super.paint(g, p);
      }
   }
   
   public Particle clone(double tx, double ty, double tz, double tdx, double tdy, double tdz){
      return new Particle(tx, ty, tz, tdx, tdy, tdz, effect);
   }
   
   public Particle clone(double tx, double ty, double tz, double tdx, double tdy, double tdz, boolean rot, boolean collide){
      return new Particle(tx, ty, tz, tdx, tdy, tdz, effect, rot, collide);
   }
   
   public SpinParticle spinclone(double tx, double ty, double tz, double tdx, double tdy, double tdz){
      return new SpinParticle(tx, ty, tz, tdx, tdy, tdz, effect);
   }
   
   public SpinParticle spinclone(double tx, double ty, double tz, double tdx, double tdy, double tdz, boolean rot, boolean collide){
      return new SpinParticle(tx, ty, tz, tdx, tdy, tdz, effect, rot, collide);
   }
   
   
   public void update(){
      super.update();
      if(!sprite.acting())
         cleanup();
   }
   
   public void physics(){
      //Do nothing
   }
   
   @Override
   public void onCollision(Entity other) {
      //Do nothing
   }
   
}
