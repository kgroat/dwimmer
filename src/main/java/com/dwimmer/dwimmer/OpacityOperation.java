/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

/**
 *
 * @author kevingroat
 */
public class OpacityOperation{

   public static BufferedImage filter(BufferedImage src, double tMult) {
      float mult = (float)tMult;
      //Do stuff
      BufferedImage dst = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_ARGB);
      Graphics g = dst.getGraphics();
      g.drawImage(src, 0, 0, null);
      WritableRaster r = dst.getAlphaRaster();
      int[] alphas = new int[dst.getWidth()*dst.getHeight()];
      r.getPixels(0, 0, dst.getWidth(), dst.getHeight(), alphas);
      for(int i=0; i<alphas.length; i++){
         alphas[i] = (int)(alphas[i]*mult);
      }
      return dst;
   }
   
}
