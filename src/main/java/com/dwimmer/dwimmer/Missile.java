/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import com.dwimmer.dwimmer.audio.SoundSource;

/**
 *
 * @author kevingroat
 */
public abstract class Missile extends Entity {

   protected int damage;
   protected int lifespan;
   protected Particle particle;
   protected int particleWait;
   public static final int pEvery = 3;

   public void update() {
      updateSound();
      if(particle != null && particleWait <= 0){
         DwimmerEngine.add(particle.clone(x+(Math.random()*hitBox.width)-particle.width()/2,
                                          y+(Math.random()*hitBox.height)-particle.width()/2,
                                          z, dx/10, dy/10, 1));
         particleWait = pEvery;
      }
      particleWait--;
      super.update();
      lifespan--;
      if(lifespan <= 0){
         explode();
         cleanup();
      }
   }
   
   @Override
   protected void physics() {
      //throw new UnsupportedOperationException("Not supported yet.");
   }
   
   public void cleanup(){
      super.cleanup();
      explode();
   }
   
   public abstract void explode();
   
}
