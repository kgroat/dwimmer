/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

/**
 *
 * @author kevingroat
 */
public abstract class Being extends Entity {
   protected int[] gems;
   
   public Being(){
      gems = new int[5];
   }
   
   public Being(double tx, double ty){
      this();
      x = tx; y = ty;
      targetable=true;
   }
   
   public void collect(String tType, int count){
      String type = tType.toLowerCase();
      gems[ColorType.valueOf(type).ordinal()] += count;
   }
   
   public void damage(int amount){
      gems[0] -= amount;
      if(gems[0] <= 0){
         this.cleanup();
      }
   }
   
}
