package com.dwimmer.dwimmer;

import java.util.List;

public class RandomUtils {
    public static <T> T chooseRandom(List<T> list){
        return list.get((int)(Math.random() * list.size()));
    }
}
