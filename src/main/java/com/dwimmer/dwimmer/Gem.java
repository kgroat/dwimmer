/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import com.dwimmer.dwimmer.audio.SoundSource;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author kevingroat
 */
public class Gem extends Entity {
   private static ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
   private static final double DRAG = .95;
   private static final double MIN = .1;
   private static final double ACCEL = 10*(1-DRAG);
   protected String color;
   protected ColorType cType;
   private int count;
   private int lifespan;
   private Wizard target;
   private SoundSource ding;
   private boolean collected;
   
   public Gem(String tag, int tCount, double tx, double ty){
      sprite = DwimmerEngine.gems.clone();
      sprite.enact(tag);
      int tmp = (int)(Math.random()*8);
      for(int i=0; i<tmp; i++){
         sprite.update();
      }
      count = tCount;
      color = tag;
      x = tx; y = ty;
      z = (Math.random()*3);
      double theta = Math.random()*Math.PI*2;
      double vel = Math.random()*5;
      dx = Math.cos(theta)*vel;
      dy = Math.sin(theta)*vel;
      dz = (Math.random()*10)+5;
      ddz = GRAV;
      bounciness = .5;
      hitBox = new Rectangle((int)x, (int)y, 9, 9);
      lifespan = 300+(int)(Math.random()*60);
      cType = ColorType.valueOf(tag);
      ding = new SoundSource(false, true);
      ding.setBuffer(RandomUtils.chooseRandom(DwimmerEngine.dings));
      sounds.add(ding);
      collected = false;
   }
   
   public Gem(int i, int tCount, double tx, double ty){
      this(ColorType.values()[i%ColorType.values().length].name(), tCount, tx, ty);
   }
   
   public void update(){
      super.update();
      lifespan--;
      if(lifespan <= 0)
         cleanup();
   }
   
   public void paint(Graphics g, Point corner){
      if (collected) {
         return;
      }
      if(lifespan < 30){
         if(lifespan % 2 == 0){
            super.paint(g, corner);
         }
      }else if(lifespan < 60){
         if(lifespan/3 % 2 == 0){
            super.paint(g, corner);
         }
      }else if(lifespan < 120){
         if(lifespan/5 % 2 == 0){
            super.paint(g, corner);
         }
      }else{
         super.paint(g, corner);
      }
   }
   
   @Override
   protected void physics() {
      super.physics();
      dx *= DRAG;
      dy *= DRAG;
      z = Math.max(0, z);
      if(Math.abs(dx) + Math.abs(dy) < MIN) 
         dx = dy = 0;
      if(target == null)
         target = DwimmerEngine.closestWizard(this);
      if(target != null){
         double dist = Math.sqrt(distSq(target));
         dx += (110-dist)/30*ACCEL*xDir(target);
         dy += (110-dist)/30*ACCEL*yDir(target);
         if(dx == 0 || dy == 0){
            System.out.println("HERE: "+dx + " / " + dy);
         }
         if(dist > 100 || target.isDisposable()) target = null;
      }
   }

   @Override
   public void onCollision(Entity other) {
      if(other instanceof Wizard && !collected){
         collected = true;
         ((Wizard)other).collect(color, count);
         ding.play();
         executor.schedule(this::cleanup, 1, TimeUnit.SECONDS);
      }
   }
   
}
