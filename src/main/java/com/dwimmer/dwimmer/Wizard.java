/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import com.dwimmer.dwimmer.audio.SoundSource;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.PrintStream;

/**
 *
 * @author kevingroat
 */
public class Wizard extends Being{
   
   boolean W, A, S, D, I, DI, J, K, DK, L, DL, SP;
   
   double angle, pow;
   
   public static final double dAng = Math.PI/4;
   
   public static final int MISSILE_PER = 2;
   public static final double DRAG = .8;
   public static final double ACCELERATION = 5*(1-DRAG);
   public static final double MIN = .2;
   protected int wait;
   protected int basicDelay;
   protected int charge;
   protected SpriteSet orb;
   public ColorType type;
   public String name;
   public static final int[] minGems = {0, 1, 10, 10, 40};
   //Expected top speed = 150 px per second @ 30 FPS

   private boolean chargingUp;
   private SoundSource chargeUp, fullCharge;

   public Wizard(){
      this(0, 0);
   }
   
   public Wizard(double tx, double ty){
      super(tx, ty);
      wait = 0;
      offsetX = 4;
      offsetY = 24;
      hitBox = new Rectangle((int)tx, (int)ty, 24, 16);
      sprite = DwimmerEngine.dwarf.clone();
      orb = DwimmerEngine.spell.clone();
      gems = new int[]{500, 100, 100, 100, 100};
      targeted = 0;
      type = ColorType.white;
      basicDelay = 10;
      chargingUp = false;

      chargeUp = new SoundSource(false, true);
      chargeUp.setBuffer(DwimmerEngine.chargeUp);
      fullCharge = new SoundSource(true, true);
      fullCharge.setBuffer(DwimmerEngine.fullCharge);

      this.sounds.add(chargeUp);
      this.sounds.add(fullCharge);
   }
   
   public void paint(Graphics g, Point corner){
      if(sprite != null){
         String enact;
         paintShadow(g, corner);
         
         if(isCasting()){
            enact = "cast";
         }else{
            enact = "walk";
         }
         
         if(fy < 0){
            enact += "up";
         }else{
            enact += "down";
         }
         
         if(dx == 0 && dy == 0){
            enact += "s";
         }
         
         if(targeted > 9){
            enact += "t";
         }
         
         sprite.enact(enact);
         
         if(fx > 0){
            sprite.flipVertDraw(g, corner.x, corner.y);
         }else{
            sprite.draw(g, corner.x, corner.y);
         }
         if(I){
            orb.enact("blue");
         }else if(K){
            orb.enact("yellow");
         }
         if(I||K){
            int orbx, orby, orbw, orbh;
            orbw = orb.getSpriteWidth() * (10+charge)/110;
            orbh = orb.getSpriteHeight() * (10+charge)/110;
            orbx = corner.x + (orb.getSpriteWidth() - orbw)/2;
            orby = corner.y - orbh - 3;
            orb.draw(g, orbx, orby, orbw, orbh);
         }
      }else{
         super.paint(g, corner);
      }
      if(drawVector)
         paintVector(g, corner);
   }
   
   public void update(){
      if(W && !S){
         pow = 1;
         if(A && !D){
            angle = -dAng*3;
         }else if(D && !A){
            angle = -dAng;
         }else{
            angle = -dAng*2;
         }
      }else if(S && !W){
         pow = 1;
         if(A && !D){
            angle = dAng*3;
         }else if(D && !A){
            angle = dAng;
         }else{
            angle = dAng*2;
         }
      }else{
         if(A && !D){
            pow = 1;
            angle = dAng*4;
         }else if(D && !A){
            pow = 1;
            angle = 0;
         }else{
            pow = 0;
         }
      }
      
      dx += pow*Math.cos(angle)*ACCELERATION;
      dy += pow*Math.sin(angle)*ACCELERATION;
//      
//      if(A && !D){
//         dx -= ACCELERATION;
//      }else if(D && !A){
//         dx += ACCELERATION;
//      }
      orb.update();
      if(wait <= 0){
         if(SP){
            wait = basicDelay;
            DwimmerEngine.add(new BasicMissile(x+16+fx*10, y+16+fy*22, fx*7.5, fy*7.5, team));
         }
         if(I && (type == ColorType.white || type == ColorType.blue)){
            //Missile swarm
            if(gems[3] > 0 && charge < 100){
               charge ++;
               gems[3] --;
            }
         }
         if(J && (type == ColorType.white || type == ColorType.red)){
            //Flamethrower
            if(gems[1] > 0){
               gems[1] --;
               DwimmerEngine.add(new Flame(centerX()+fx*10, centerY()-16+fy*10, fx*11+Math.random()*5-2.5+dx, fy*11+Math.random()*5-2.5+dy, team));
            }
         }
         if(K && (type == ColorType.white || type == ColorType.yellow)){
            //Shockwave
            if (chargingUp) {
               if (!chargeUp.isPlaying()) {
                  fullCharge.play();
               }
            }
            chargingUp = true;
            if (!chargeUp.isPlaying()) {
               chargeUp.play();
            }
            if(gems[4] > 0 && charge < 100){
               charge ++;
               gems[4] --;
            }
         } else {
            chargingUp = false;
            if (chargeUp.isPlaying()) {
               chargeUp.stop();
            }
            if (fullCharge.isPlaying()) {
               fullCharge.stop();
            }
         }
         if(L && (type == ColorType.white || type == ColorType.green)){
            //Birds
            if(gems[2] > 0 && charge < 10){
               charge ++;
               gems[2] --;
               DwimmerEngine.add(new SpinParticle(centerX()+13, centerY(), z+60, Math.random()*2-1, Math.random()*2-1, Math.random()*2, "feather", true, true));
               DwimmerEngine.add(new SpinParticle(centerX()-13, centerY(), z+60, Math.random()*2-1, Math.random()*2-1, Math.random()*2, "feather", true, true));
            }else if(charge == 10){
               charge = 0;
               DwimmerEngine.add(new Bird(this));
               for(int i=0; i<5; i++){
                  DwimmerEngine.add(new SpinParticle(centerX(), centerY(), z+70, Math.random()*2-1, Math.random()*2-1, Math.random()*2, "feather", true, true));
               }
            }
         }
         if(DI && (type == ColorType.white || type == ColorType.blue)){
            for(int i=0; i<charge/MISSILE_PER; i++){
               DwimmerEngine.add(new HomingMissile(centerX(), trueY(), team));
            }
            gems[3] += charge%MISSILE_PER;
            charge = 0;
         }
         if(DK && (type == ColorType.white || type == ColorType.yellow)){
            //Release shockwave
            if(charge > 10)
               DwimmerEngine.add(new Shockwave(centerX(), (centerY()+y)/2, charge, team));
            else
               gems[4] += charge;
            charge = 0;
         }
         if(DL && (type == ColorType.white || type == ColorType.green)){
            //Release birds
            gems[2] += charge;
            charge = 0;
         }
      }
      wait--;
      DL = false;
      DI = false;
      DK = false;
      super.update();
   }
   
   @Override
   protected void physics() {
      dx *= DRAG;
      dy *= DRAG;
      if(Math.abs(dx) + Math.abs(dy) < MIN) dx = dy = 0;
   }

   @Override
   public void onCollision(Entity other) {
      if(other instanceof Being && !other.ignoreCollide && team != other.team){
         Rectangle r1 = new Rectangle(hitBox), r2 = new Rectangle(other.hitBox);
         r1.translate((int)dx, (int)dy);
         r2.translate((int)other.dx, (int)other.dy);
         double tmp;
         tmp = dx - other.dx;
         if(r1.x >= r2.x && tmp < 0|| r1.x <= r2.x && tmp > 0){
            double bigx = (dx + other.dx)/2;
            dx = other.dx = bigx;
         }
         tmp = dy - other.dy;
         if(r1.y >= r2.y && tmp < 0|| r1.y <= r2.y && tmp > 0){
            double bigy = (dy + other.dy)/2;
            dy = other.dy = bigy;
         }
      }
   }
   
   public boolean isCasting(){
      return I || J || K || L || SP;
   }
      
   public boolean hasGoodGems(){
      switch(type){
         case white:
            return gems[1]>minGems[1] || gems[2]>minGems[2] || gems[3]>minGems[3] || gems[4]>minGems[4];
         default: 
            int tmp = type.ordinal();
            return gems[tmp] > minGems[tmp];
      }
   }
   
   public boolean hasGoodGems234(){
      switch(type){
         case white:
            return gems[2]>minGems[2] ||gems[3]>minGems[3] || gems[4]>minGems[4];
         case green:
            return gems[2]>minGems[2];
         case blue:
            return gems[3]>minGems[3];
         case yellow:
            return gems[4]>minGems[4];
         default:
            return false;
      }
   }
   
   public boolean hasGoodGems34(){
      switch(type){
         case white:
            return gems[3]>minGems[3] || gems[4]>minGems[4];
         case blue:
            return gems[3]>minGems[3];
         case yellow:
            return gems[4]>minGems[4];
         default:
            return false;
      }
   }
   
   public boolean hasGoodGems134(){
      switch(type){
         case white:
            return gems[1]>minGems[1] ||gems[3]>minGems[3] || gems[4]>minGems[4];
         case red:
            return gems[1]>minGems[1];
         case blue:
            return gems[3]>minGems[3];
         case yellow:
            return gems[4]>minGems[4];
         default:
            return false;
      }
   }
   
   public void cleanup(){
      super.cleanup();
      if(I || DI){
         gems[3]+=charge;
      }else if(K || DK){
         gems[4]+=charge;
      }else if(L || DL){
         gems[2]+=charge;
      }
   }
   
   public void dump(PrintStream out){
      out.printf("POS:  (%+7.1f / %+7.1f)\n", x, y);
      out.printf("VEL:  (%+7.1f / %+7.1f)\n", dx, dy);
      out.printf("FACE: (%+7.1f / %+7.1f)\n", fx, fy);
      out.printf("TEAM: (%6d)\n", team);
      out.printf("GEMS: (%d, %d, %d, %d, %d)\n", gems[0], gems[1], gems[2], gems[3], gems[4]);
      out.printf("WASD: (%b, %b, %b, %b)\n", W, A, S, D);
      out.printf("IJKL: (%b, %b, %b, %b)\n", I, J, K, L);
      out.printf("DIKL: (%b, %b, %b)\n", DI, DK, DL);
      out.printf("SP:   (%b)\n", SP);
   }
   
}
