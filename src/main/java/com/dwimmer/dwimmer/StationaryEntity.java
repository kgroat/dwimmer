/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import java.awt.Rectangle;

/**
 *
 * @author kevingroat
 */
public class StationaryEntity extends Entity{
   
   
   public StationaryEntity(double tx, double ty){
      x = tx; y = ty;
      ignoreCollide = false;
      offsetX = -8;
      offsetY = -8;
      hitBox = new Rectangle((int)x-offsetX, (int)y-offsetY, -offsetX*2, -offsetY*2);
   }

   @Override
   public void onCollision(Entity other) {
      if(other instanceof Missile){
         System.out.println("HIT");
         this.cleanup();
      }
   }
}
