/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import java.awt.event.KeyEvent;

/**
 *
 * @author kevingroat
 */
public class InputHelper {
   
   private static int UP = KeyEvent.VK_W;
   private static int DOWN = KeyEvent.VK_S;
   private static int LEFT = KeyEvent.VK_A;
   private static int RIGHT = KeyEvent.VK_D;
   private static int FIRE = KeyEvent.VK_J;
   private static int MISSILES = KeyEvent.VK_I;
   private static int BIRDS = KeyEvent.VK_L;
   private static int SHOCKWAVE = KeyEvent.VK_K;
   private static int BASIC = KeyEvent.VK_SPACE;
   private static int CURRENT = KeyEvent.VK_PERIOD;
   private static int SWAPLEFT = KeyEvent.VK_COMMA;
   private static int SWAPRIGHT = KeyEvent.VK_SLASH;
 
   public static int W(){
      return UP;
   }
   
   public static int S(){
      return DOWN;
   }
   
   public static int A(){
      return LEFT;
   }
   
   public static int D(){
      return RIGHT;
   }
   
   public static int J(){
      return FIRE;
   }
   
   public static int I(){
      return MISSILES;
   }
   
   public static int L(){
      return BIRDS;
   }
   
   public static int K(){
      return SHOCKWAVE;
   }
   
   public static int SP(){
      return BASIC;
   }
   
   public static int PD(){
      return CURRENT;
   }
   
   public static int CM(){
      return SWAPLEFT;
   }
   
   public static int SL(){
      return SWAPRIGHT;
   }
   
   public static int getCheck(int kc){
      if(kc == W()){
         return KeyEvent.VK_W;
      }else if(kc == S()){
         return KeyEvent.VK_S;
      }else if(kc == A()){
         return KeyEvent.VK_A;
      }else if(kc == D()){
         return KeyEvent.VK_D;
      }else if(kc == J()){
         return KeyEvent.VK_J;
      }else if(kc == I()){
         return KeyEvent.VK_I;
      }else if(kc == L()){
         return KeyEvent.VK_L;
      }else if(kc == K()){
         return KeyEvent.VK_K;
      }else if(kc == SP()){
         return KeyEvent.VK_SPACE;
      }else if(kc == PD()){
         return KeyEvent.VK_PERIOD;
      }else if(kc == CM()){
         return KeyEvent.VK_COMMA;
      }else if(kc == SL()){
         return KeyEvent.VK_SLASH;
      }else{
         return kc;
      }
   }
}
