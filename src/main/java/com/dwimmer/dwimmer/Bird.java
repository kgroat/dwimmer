/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

/**
 *
 * @author kevingroat
 */
public class Bird extends Being{

   private double theta, vel;
   protected Wizard target;
   protected Entity target2;
   private static final double accel = 1.02;
   private static final double decel = 0.98;
   private static final double maxVel = 10;
   private static final double minVel = 2.5;
   private static final double ddz = .2;
   private static final double maxdz = 2;
   private static final double mindz = -maxdz;
   private static final double minz = 16;
   private static final double maxz = 64;
   private static final double maxTurn = Math.PI/20;
   private static final double primeDist = 100;
   private static final double dvar = primeDist / 3;
   private static final int START_LIFE = 900;
   private static final SpinParticle feather = new SpinParticle("feather");
   protected double var;
   protected int lifespan;
   
   public Bird(Wizard origin){
      sprite = DwimmerEngine.birds.clone();
      sprite.enact("upf");
      hitBox = new Rectangle((int)x, (int)y, 16, 16);
      x = origin.centerX()-8;
      y = origin.centerY()-8;
      z = origin.z + 100;
      theta = Math.random()*Math.PI*2;
      vel = 2.5+Math.random()*5;
      dx = Math.cos(theta)*vel;
      dy = Math.sin(theta)*vel;
      dz = -1;
      targetable = true;
      team = origin.team;
      target = origin;
      acquireTarget();
      gems = new int[]{50, 0, 0, 0, 0};
      var = primeDist+Math.random()*2*dvar-dvar;
      lifespan = START_LIFE;
   }
   
   public void paint(Graphics g, Point corner){
      paintShadow(g, corner);
      String enact = "";
      if(fy < 0){
         enact = "up";
      }else{
         enact = "down";
      }
      if(sprite.acting()) enact += "f";
      sprite.enact(enact);
      if(fx < 0){
            sprite.flipVertDraw(g, corner.x, corner.y-(int)z/2);
      }else{
         sprite.draw(g, corner.x, corner.y-(int)z/2);
      }
      if(drawVector)
         paintVector(g, corner);
   }
   
   @Override
   public void update(){
      if(target2 == null || target.distSq(target2) > 2*primeDist || target2.isDisposable())
         acquireTarget();
      
      if(target2 != null){
         if(isCloser2(0, 0)){
            vel = Math.max(minVel, vel*decel);
            dz = Math.min(maxdz, dz+ddz);
         }else{
            vel = Math.min(maxVel, vel*accel);
            dz = Math.max(mindz, dz-ddz);
         }
      }else{
         if(isCloser(0, 0)){
            vel = Math.max(minVel, vel*decel);
            dz = Math.min(maxdz, dz+ddz);
         }else{
            vel = Math.min(maxVel, vel*accel);
            dz = Math.max(mindz, dz-ddz);
         }
      }
      
      dx = Math.cos(theta)*vel;
      dy = Math.sin(theta)*vel;
      
      if(target2 != null){
         double tdx, tdy, ttheta;
         ttheta = (theta + maxTurn) % (Math.PI*2);
         tdx = Math.cos(ttheta)*vel;
         tdy = Math.sin(ttheta)*vel;
         if(isCloser2(tdx, tdy)){
            theta = ttheta;
            dx = tdx;
            dy = tdy;
         }
         ttheta = (theta - maxTurn) % (Math.PI*2);
         tdx = Math.cos(ttheta)*vel;
         tdy = Math.sin(ttheta)*vel;
         if(isCloser2(tdx, tdy)){
            theta = ttheta;
            dx = tdx;
            dy = tdy;
         }
      }else{
         double tdx, tdy, ttheta;
         ttheta = (theta + maxTurn) % (Math.PI*2);
         tdx = Math.cos(ttheta)*vel;
         tdy = Math.sin(ttheta)*vel;
         if(isCloser(tdx, tdy)){
            theta = ttheta;
            dx = tdx;
            dy = tdy;
         }
         ttheta = (theta - maxTurn) % (Math.PI*2);
         tdx = Math.cos(ttheta)*vel;
         tdy = Math.sin(ttheta)*vel;
         if(isCloser(tdx, tdy)){
            theta = ttheta;
            dx = tdx;
            dy = tdy;
         }
      }
      super.update();
      z = Math.max(minz, Math.min(z, maxz));
      lifespan--;
      if(lifespan<1){
         cleanup();
      }
   }
   
   private boolean isCloser(double tdx, double tdy){
      return Math.abs(target.distSq(centerX()+dx, centerY()+dy)-var*var) > 
             Math.abs(target.distSq(centerX()+tdx, centerY()+tdy)-var*var);
   }
   
   private boolean isCloser2(double tdx, double tdy){
      return target2.distSq(centerX()+dx, centerY()+dy) > target2.distSq(centerX()+tdx, centerY()+tdy);
   }
   
   public void explode() {
      for(int i=0; i<10; i++){
         DwimmerEngine.add(new SpinParticle(centerX(), centerY(), z, Math.random()*2-1+dx, Math.random()*2-1+dy, Math.random()*2, "feather", true, true));
      }
   }

   @Override
   public void onCollision(Entity other) {
      if(other.team != team){
         if(other instanceof Being){
            ((Being)other).damage(2);
         }
      }
   }
   
   private void acquireTarget(){
      if(target2 != null){
         target2.targeted --;
      }
      target2 = null;
      if(this.distSq(target) < 4*primeDist*primeDist)
         target2 = DwimmerEngine.closestTarget(this, primeDist);
      
      if(target2 == null){
         target2 = DwimmerEngine.closestTarget(target, primeDist);
      }
      if(target2 != null)
         target2.targeted++;
   }
   
   public void cleanup(){
      if(!isDisposable()){
         explode();
         if(target2 != null)
            target2.targeted--;
      }
      super.cleanup();
   }
   
}
