/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import com.dwimmer.dwimmer.audio.SoundBuffer;
import com.dwimmer.dwimmer.audio.SoundListener;
import com.dwimmer.dwimmer.audio.SoundManager;
import com.dwimmer.dwimmer.audio.SoundSource;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.Timer;
import java.io.File;
import java.util.List;
import javax.imageio.ImageIO;

/**
 *
 * @author kevingroat
 */
public final class DwimmerEngine {
   
   public static final SpriteSet gems, dwarf, spell, particles, fire, birds;

   public static final int DELAY = 33;
   private static ArrayList<Entity> entities, targetables, collidables;
   private static ArrayList<Shockwave> shockwaves;
   private static ArrayList<Wizard> wizards;
   private static ArrayList<Gem> liveGems;
   private static Player player;
   private static boolean playing = false;
   private static BufferedImage image, bg, iface, iPress, jPress, kPress, lPress, map;
   private static int width, height, gframe;
   private static Timer thinker, painter;
   private static boolean isF1Down = false, isF2Down = false, isF3Down = false, isF4Down = false, isF5Down = false, isF6Down = false, isF7Down = false, isF8Down = false,
           iDown, jDown, kDown, lDown, isGDown, isMDown, renderGems = true, saveImage = false, isEnterDown = false;
   private static int renderMap = 1, imgcnt = 0;
   private static boolean[][][] mapBits;
   
   //Easy-access wrapping and map values
   public static final double SIZE = 3200;
   public static final double MAP_COVERAGE = 1600;
   public static final int MAP_RENDER_SIZE = 80;
   
   //WARNING: DO NOT CHANGE THE VALUES BELOW
   public static final double HALF_SIZE = SIZE/2;
   public static final double HALF_MAP_COVERAGE = MAP_COVERAGE/2;
   public static final int HALF_MAP_RENDER_SIZE = MAP_RENDER_SIZE/2;
   public static final double MAP_DIVISOR = MAP_COVERAGE / MAP_RENDER_SIZE;
   public static final double MAP_MOD = SIZE/MAP_DIVISOR;

   public static SoundListener soundListener;
   public static SoundManager soundManager;
   public static SoundBuffer entertainer,
           blast1, blast2, blast3,
           chargeUp, fullCharge,
           ding1, ding2, ding3, ding4, ding5,
           fire1, fire2, fire3, fire4, fire5;
   public static List<SoundBuffer> blasts, dings, fires;

   static {
      gems = SpriteSet.load("/sprites/gems.txt");
      dwarf = SpriteSet.load("/sprites/dwarfy.txt");
      spell = SpriteSet.load("/sprites/spell.txt");
      particles = SpriteSet.load("/sprites/particles.txt");
      fire = SpriteSet.load("/sprites/fire.txt");
      birds = SpriteSet.load("/sprites/birds.txt");
   }

   private DwimmerEngine() {
      //DO NOT USE
   }

   public static void start() throws Exception {
//      AudioClip.setSubGain(AudioClip.ClipType.music, 1.0f);
//      AudioClip.setSubGain(AudioClip.ClipType.sfx, 0.8f);
//      AudioClip.setMasterGain(0.8f);
//      AudioClip.get("ent.ogg").forcePlay(false, false);
//      System.out.println(AudioClip.get("ent.ogg").isPlaying());
      initSound();
      iface = FileUtility.loadImage("/sprites/interface.png");
      bg = FileUtility.loadImage("/sprites/bg.png");
      iPress = FileUtility.loadImage("/sprites/i.png");
      jPress = FileUtility.loadImage("/sprites/j.png");
      kPress = FileUtility.loadImage("/sprites/k.png");
      lPress = FileUtility.loadImage("/sprites/l.png");
      
      map = new BufferedImage(MAP_RENDER_SIZE+2, MAP_RENDER_SIZE+2, BufferedImage.TYPE_INT_RGB);
      mapBits = new boolean[MAP_RENDER_SIZE][MAP_RENDER_SIZE][2];
      width = FullScreenView.instance().getScreenWidth() / 2;
      height = FullScreenView.instance().getScreenHeight() / 2;
      image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
      entities = new ArrayList<Entity>();
      targetables = new ArrayList<Entity>();
      collidables = new ArrayList<Entity>();
      wizards = new ArrayList<Wizard>();
      liveGems = new ArrayList<Gem>();
      shockwaves = new ArrayList<Shockwave>();
      player = new Player();
      add(player);
      for (int i = 0; i < 50; i++) {
         add(new AIWizard(Math.random() * SIZE, Math.random() * SIZE, i));
      }
      playing = true;
      thinker = new Timer(DELAY, ae -> {
         if (playing) {
            update();
         }
      });
      painter = new Timer(DELAY, ae -> {
         render();
         FullScreenView.instance().drawImage(image);
      });
      painter.setInitialDelay(0);
      painter.start();
      thinker.start();
   }

   private static void initSound() throws Exception {
      soundManager = new SoundManager();
      soundManager.init();
      soundListener = new SoundListener();
      soundManager.setListener(soundListener);

      entertainer = new SoundBuffer("/audio/ent.ogg");

      blast1 = new SoundBuffer("/audio/Blast_1.ogg", true);
      blast2 = new SoundBuffer("/audio/Blast_2.ogg", true);
      blast3 = new SoundBuffer("/audio/Blast_3.ogg", true);
      blasts = new ArrayList<>();
      blasts.add(blast1);
      blasts.add(blast2);
      blasts.add(blast3);

      chargeUp = new SoundBuffer("/audio/Chargeup.ogg", true);
      fullCharge = new SoundBuffer("/audio/FullCharge.ogg", true);

      ding1 = new SoundBuffer("/audio/Ding_1.ogg", true);
      ding2 = new SoundBuffer("/audio/Ding_2.ogg", true);
      ding3 = new SoundBuffer("/audio/Ding_3.ogg", true);
      ding4 = new SoundBuffer("/audio/Ding_4.ogg", true);
      ding5 = new SoundBuffer("/audio/Ding_5.ogg", true);
      dings = new ArrayList<>();
      dings.add(ding1);
      dings.add(ding2);
      dings.add(ding3);
      dings.add(ding4);
      dings.add(ding5);

      fire1 = new SoundBuffer("/audio/Fire_1.ogg", true);
      fire2 = new SoundBuffer("/audio/Fire_2.ogg", true);
      fire3 = new SoundBuffer("/audio/Fire_3.ogg", true);
      fire4 = new SoundBuffer("/audio/Fire_4.ogg", true);
      fire5 = new SoundBuffer("/audio/Fire_5.ogg", true);
      fires = new ArrayList<>();
      fires.add(fire1);
      fires.add(fire2);
      fires.add(fire3);
      fires.add(fire4);
      fires.add(fire5);

      SoundSource music = new SoundSource(true, false);
      music.setBuffer(entertainer.getBufferId());
      music.play();
      soundManager.addSoundSource("music", music);
   }

   public static void pressKey(KeyEvent ke) {
      switch (InputHelper.getCheck(ke.getKeyCode())) {
         case KeyEvent.VK_W:
            player.W = true;
            break;

         case KeyEvent.VK_S:
            player.S = true;
            break;

         case KeyEvent.VK_A:
            player.A = true;
            break;

         case KeyEvent.VK_D:
            player.D = true;
            break;

         case KeyEvent.VK_I:
            if (!player.isCasting()) {
               player.I = true;
            }
            iDown = true;
            break;

         case KeyEvent.VK_J:
            if (!player.isCasting()) {
               player.J = true;
            }
            jDown = true;
            break;

         case KeyEvent.VK_K:
            if (!player.isCasting()) {
               player.K = true;
//               AudioClip.get("Chargeup.ogg").forcePlay(false, false);
//               AudioClip.get("FullCharge.ogg").startAfter("Chargeup.ogg", true, false, false);
            }
            kDown = true;
            break;

         case KeyEvent.VK_L:
            if (!player.isCasting()) {
               player.L = true;
            }
            lDown = true;
            break;

         case KeyEvent.VK_SPACE:
            if (!player.isCasting()) {
               player.SP = true;
            }
            break;

         case KeyEvent.VK_G:
            if (!isGDown) {
               isGDown = true;
               renderGems = !renderGems;
            }
            break;
         case KeyEvent.VK_M:
            if (!isMDown) {
               isMDown = true;
               renderMap = (renderMap + 1) % 3;
            }
            break;

         case KeyEvent.VK_F1:
            if (!isF1Down) {
               isF1Down = true;
               Entity.drawVector = !Entity.drawVector;
            }
            break;
         case KeyEvent.VK_F2:
            if (!isF2Down) {
               isF2Down = true;
               add(new AIWizard(player.x + 50, player.y, 0));
            }
            break;
         case KeyEvent.VK_F3:
            if (!isF3Down) {
               isF3Down = true;
            }
            break;
         case KeyEvent.VK_F4:
            if (!isF4Down) {
               isF4Down = true;
            }
            break;
         case KeyEvent.VK_F5:
            if (!isF5Down) {
               isF5Down = true;
               chooseNext();
            }
            break;
         case KeyEvent.VK_F6:
            if (!isF6Down) {
               isF6Down = true;
               printWizards();
               player.dump(System.out);
            }
            break;
         case KeyEvent.VK_F7:
            if (!isF7Down) {
               isF7Down = true;
            }
            break;
         case KeyEvent.VK_F8:
            if (!isF8Down) {
               isF8Down = true;
               saveImage = true;
            }
            break;
         case KeyEvent.VK_ENTER:
            if (!isEnterDown) {
               isEnterDown = true;
//               testAudio();
            }
            break;
      }
   }

   public static void releaseKey(KeyEvent ke) {
      switch (InputHelper.getCheck(ke.getKeyCode())) {
         case KeyEvent.VK_W:
            player.W = false;
            break;

         case KeyEvent.VK_S:
            player.S = false;
            break;

         case KeyEvent.VK_A:
            player.A = false;
            break;

         case KeyEvent.VK_D:
            player.D = false;
            break;

         case KeyEvent.VK_I:
            player.DI = player.I;
            player.I = false;
            iDown = false;
            trySpells();
            break;

         case KeyEvent.VK_J:
            player.J = false;
            jDown = false;
            trySpells();
            break;

         case KeyEvent.VK_K:
            player.DK = player.K;
            player.K = false;
            kDown = false;
            trySpells();
            break;

         case KeyEvent.VK_L:
            player.DL = player.L;
            player.L = false;
            lDown = false;
            trySpells();
            break;

         case KeyEvent.VK_SPACE:
            player.SP = false;
            trySpells();
            break;

         case KeyEvent.VK_G:
            isGDown = false;
            break;
         case KeyEvent.VK_M:
            isMDown = false;
            break;

         case KeyEvent.VK_F1:
            isF1Down = false;
            break;
         case KeyEvent.VK_F2:
            isF2Down = false;
            break;
         case KeyEvent.VK_F3:
            isF3Down = false;
            break;
         case KeyEvent.VK_F4:
            isF4Down = false;
            break;
         case KeyEvent.VK_F5:
            isF5Down = false;
            break;
         case KeyEvent.VK_F6:
            isF6Down = false;
            break;
         case KeyEvent.VK_F7:
            isF7Down = false;
            break;
         case KeyEvent.VK_F8:
            isF8Down = false;
            break;
         case KeyEvent.VK_ENTER:
            isEnterDown = false;
            break;
      }
   }

   public static void chooseNext() {
      int tmp = wizards.indexOf(player);
      System.out.println("Yay" + tmp);
      if (tmp < 0) {
         tmp = 0;
      }
      Wizard wiz;
      for (int i = 1; i < wizards.size() + 1; i++) {
         wiz = wizards.get((i + tmp) % wizards.size());
         if (wiz instanceof Player) {
            player = (Player) wiz;
            if (wiz.I) {
               wiz.gems[3] += wiz.charge;
            } else if (wiz.K) {
               wiz.gems[4] += wiz.charge;
            } else if (wiz.L) {
               wiz.gems[2] += wiz.charge;
            }
            wiz.charge = 0;
            wiz.W = wiz.A = wiz.S = wiz.D =
                    wiz.I = wiz.J = wiz.K = wiz.L =
                    wiz.DI = wiz.DK = wiz.DL = false;
            return;
         }
      }
   }

   public static Entity closestTarget(Entity other, double far) {
      Entity out = null, curr = null;
      double best = far * far, dist, ydist = 0;
      int p, start = targetables.indexOf(other);
      if (start < 0) {
         for (int i = 0; i < targetables.size(); i++) {
            curr = targetables.get(i);
            ydist = curr.yDist(other);
            dist = curr.distSq(other);
            if (dist < best && curr.team != other.team) {
               best = dist;
               out = curr;
            }
         }
      } else {
         ydist = 0;
         p = start + 1;
         while (ydist < far && p < targetables.size()) {
            curr = targetables.get(p);
            ydist = curr.yDist(other);
            dist = curr.distSq(other);
            if (dist < best && curr.team != other.team) {
               best = dist;
               out = curr;
            }
            p++;
         }
         ydist = 0;
         p = start - 1;
         while (ydist < far && p > -1) {
            curr = targetables.get(p);
            ydist = curr.yDist(other);
            dist = curr.distSq(other);
            if (dist < best && curr.team != other.team) {
               best = dist;
               out = curr;
            }
            p--;
         }
      }
      return out;
   }

   public static Entity weightedTarget(Entity other, double far, double cutoff) {
      Entity out = null, curr = null;
      int outWeight = 0, currWeight;
      double best = far * far, dist, ydist = 0;
      int p, start = targetables.indexOf(other);
      cutoff = cutoff*cutoff;
      if (start < 0) {
         for (int i = 0; i < targetables.size(); i++) {
            curr = targetables.get(i);
            if (curr.team != other.team) {
               if (curr instanceof Wizard) {
                  currWeight = 5;
               } else if (curr instanceof HomingMissile) {
                  currWeight = 4;
               } else if (curr instanceof Being) {
                  currWeight = 3;
               } else if (curr instanceof Missile) {
                  currWeight = 2;
               } else {
                  currWeight = 1;
               }
               dist = curr.distSq(other);
               if ((dist < best && (currWeight >= outWeight || dist < cutoff)) || currWeight > outWeight) {
                  best = dist;
                  out = curr;
                  outWeight = currWeight;
               }
            }
         }
      } else {
         ydist = 0;
         p = start + 1;
         while (ydist < far && p < targetables.size()) {
            curr = targetables.get(p);
            ydist = curr.yDist(other);
            if (curr.team != other.team) {
               if (curr instanceof Wizard) {
                  currWeight = 5;
               } else if (curr instanceof HomingMissile) {
                  currWeight = 4;
               } else if (curr instanceof Being) {
                  currWeight = 3;
               } else if (curr instanceof Missile) {
                  currWeight = 2;
               } else {
                  currWeight = 1;
               }
               dist = curr.distSq(other);
               if ((dist < best && (currWeight >= outWeight || dist < cutoff)) || (currWeight > outWeight && dist < far*far)) {
                  best = dist;
                  out = curr;
                  outWeight = currWeight;
               }
            }
            p++;
         }
         ydist = 0;
         p = start - 1;
         while (ydist < far && p > -1) {
            curr = targetables.get(p);
            ydist = curr.yDist(other);
            if (curr.team != other.team) {
               if (curr instanceof Wizard) {
                  currWeight = 5;
               } else if (curr instanceof HomingMissile) {
                  currWeight = 4;
               } else if (curr instanceof Being) {
                  currWeight = 3;
               } else if (curr instanceof Missile) {
                  currWeight = 2;
               } else {
                  currWeight = 1;
               }
               dist = curr.distSq(other);
               if ((dist < best && (currWeight >= outWeight || dist < cutoff)) || (currWeight > outWeight && dist < far*far)) {
                  best = dist;
                  out = curr;
                  outWeight = currWeight;
               }
            }
            p--;
         }
      }
      return out;
   }

   public static Wizard closestWizard(Gem other) {
      Wizard out = null;
      Wizard curr = null;
      double best = 10000, dist;
      for (int i = 0; i < wizards.size(); i++) {
         curr = wizards.get(i);
         dist = curr.distSq(other);
         if (dist < best && (curr.type == Entity.ColorType.valueOf(other.color) || curr.type == Entity.ColorType.white)) {
            best = dist;
            out = curr;
         }
      }
      return out;
   }

   public static Gem closestGem(Wizard other, double far) {
      Gem out = null;
      Gem curr = null;
      double best = far*far, dist;
      for (int i = 0; i < liveGems.size(); i++) {
         curr = liveGems.get(i);
         dist = curr.distSq(other);
         if (dist < best && (other.type == Entity.ColorType.valueOf(curr.color) || other.type == Entity.ColorType.white)) {
            best = dist;
            out = curr;
         }
      }
      return out;
   }

   public static void add(Entity e) {
      e.init();
      while(e.x < 0){
         e.x += SIZE*10;
      }
      while(e.y < 0){
         e.y += SIZE*10;
      }
      e.x %= SIZE;
      e.y %= SIZE;
      
      if (e != null) {
         if (e.isTargetable()) {
            targetables.add(e);
         }
         if (!e.ignoreCollide) {
            collidables.add(e);
         }
         if (e instanceof Wizard) {
            wizards.add((Wizard) e);
         }
         if (e instanceof Gem) {
            liveGems.add((Gem) e);
         }
         entities.add(e);
      }
   }

   public static void add(Shockwave s) {
      shockwaves.add(s);
   }

   public static void trySpells() {
      if (iDown) {
         player.I = true;
      } else if (jDown) {
         player.J = true;
      } else if (kDown) {
         player.K = true;
      } else if (lDown) {
         player.L = true;
      }
   }

   private static void update() {
//      AudioClip.setListeningPosition((float)player.x, (float)player.y, 0);
      if (isF3Down) {
         for (int i = 1; i < 5; i++) {
            player.gems[i] += 10;
         }
      }

      if (isF4Down) {
         player.gems[0] += 50;
      }

      final double MAX_DY = 100;
      double dy;

      sortEntities();
      Entity e1, e2;
      HomingMissile m;
      Bird b;
      for (int i = 0; i < collidables.size(); i++) {
         e1 = collidables.get(i);
         if (!e1.isDisposable()) {
            dy = 0;
            for (int j = i + 1; j < collidables.size() && !e1.isDisposable() && dy < MAX_DY; j++) {
               e2 = collidables.get(j);
               dy = e1.yDist(e2);
               if (!e2.isDisposable()) {
                  if (e1.collide(e2)) {
                     e1.onCollision(e2);
                  }
                  if (e2.collide(e1)) {
                     e2.onCollision(e1);
                  }
               }
            }
         }
      }

      for (int i = 0; i < shockwaves.size(); i++) {
         for (int j = 0; j < collidables.size(); j++) {
            shockwaves.get(i).collideAndEffect(collidables.get(j));
         }
         shockwaves.get(i).update();
         if (shockwaves.get(i).isDisposable()) {
            shockwaves.remove(i);
            i--;
         }
      }

      
      
      for (int i = 0; i < entities.size(); i++) {
         e1 = entities.get(i);
         e1.update();
         e1.x = (e1.x + SIZE)%SIZE;
         e1.y = (e1.y + SIZE)%SIZE;
         if (e1.isDisposable()) {
            e2 = entities.remove(i);
            collidables.remove(e2);
            targetables.remove(e2);
            wizards.remove(e2);
            if (e2 instanceof Being) {
               dropGems((Being) e2);
            }
            i--;
         }
      }
   }

   private static void sortEntities() {
      Collections.sort(entities, new Entity.EntityComparator());
      Collections.sort(collidables, new Entity.EntityComparator());
      Collections.sort(targetables, new Entity.EntityComparator());
   }

   private static BufferedImage render() {
      sortEntities();
      Graphics2D g = image.createGraphics();
      g.setColor(Color.BLACK);
      g.fillRect(0, 0, width, height);

      int tx, ty, sx, sy;
      double dy;
      tx = ((int) -player.centerX()) % bg.getWidth();
      ty = ((int) -player.centerY()) % bg.getHeight();
      sx = (int) Math.signum(player.centerX());
      sy = (int) Math.signum(player.centerY());
      g.drawImage(bg, tx, ty, null);
      g.drawImage(bg, tx, ty + sy * bg.getHeight(), null);
      g.drawImage(bg, tx + sx * bg.getWidth(), ty, null);
      g.drawImage(bg, tx + sx * bg.getWidth(), ty + sy * bg.getHeight(), null);
      
      dy = 0;
      int place = entities.indexOf(player), pl2;
      Entity e;
      
      if(place < 0){
         place++;
         while(dy > height/2 && place < entities.size()){
            e = entities.get(place);
            dy = e.ryDist(player)-player.height();
            place++;
         }
         place--;
      }else{
         pl2 = -1;
         dy = -1;
         while(dy < 0 && pl2 > -(entities.size()-1)){
            e = entities.get((place + pl2 + entities.size())%entities.size());
            dy = e.ryRel(player);
            pl2--;
         }
         place = (place+pl2+1+entities.size())%entities.size();
      }

      for(int i=0; i<entities.size(); i++){
         e = entities.get((i+place)%entities.size());
         tx = (int)(e.rxDist(player)-player.width());
         ty = (int)(e.ryDist(player)-player.height());
         if(tx < width/2 && ty < height/2){
            e.paint(g, new Point((int)(e.rxRel(player)-player.width()/2+width/2), (int)(e.ryRel(player)-player.height()/2+height/2)));
         }
      }

      Shockwave sw;
      for (int i = 0; i < shockwaves.size(); i++) {
         sw = shockwaves.get(i);
         tx = (int) (sw.x - player.centerX() + width / 2);
         ty = (int) (sw.y - player.centerY() + height / 2);
         if(tx-sw.radius>0 && tx+sw.radius<width && ty-sw.radius>0 && ty+sw.radius<height)
            sw.render(image, tx, ty);
      }

      sx = iface.getWidth() + 3;
      sy = height - iface.getHeight() - 1;

      if (renderGems) {
         g.drawImage(iface, 2, sy - 1, null);
         gframe++;
         gems.setCurrentFrame(gframe / 3);
         String s;
         double mult;
         int count;
         for (int i = 1; i < 5; i++) {
            switch (i) {
               case 1:
                  s = "blue";
                  break;
               case 2:
                  s = "red";
                  break;
               case 3:
                  s = "yellow";
                  break;
               case 4:
                  s = "green";
                  break;
               default:
                  s = null;
            }
            gems.enact(s);
            place = Gem.ColorType.valueOf(s).ordinal();
            count = player.gems[place];
            if (player.gems[place] * 7 / 10 + sx + gems.getSpriteWidth() < width) {
               mult = 7;
            } else {
               mult = (width - sx - gems.getSpriteWidth()) * 10. / player.gems[place];
            }
            for (int j = 0; j < count; j += 10) {
               tx = (int) (j * mult / 10 + sx);
               ty = (i - 1) * 11 + sy;
               gems.draw(g, tx, ty);
            }
         }

         count = player.gems[0];
         if (player.gems[0] * 7 / 50 + sx + gems.getSpriteWidth() < width) {
            mult = 7;
         } else {
            mult = (width - sx - gems.getSpriteWidth()) * 50. / player.gems[0];
         }
         gems.enact("white");
         ty = sy - 10;
         for (int i = 0; i < count; i += 50) {
            tx = (int) (i * mult / 50 + sx + 1);
            gems.draw(g, tx, ty);
         }
         sy -= 1;
         if (player.I) {
            g.drawImage(iPress, 2, sy, null);
         } else if (player.J) {
            g.drawImage(jPress, 2, sy + 11, null);
         } else if (player.K) {
            g.drawImage(kPress, 2, sy + 22, null);
         } else if (player.L) {
            g.drawImage(lPress, 2, sy + 33, null);
         }

         if (renderMap != 0) {
            //render map
            for (int i = 0; i < MAP_RENDER_SIZE; i++) {
               for (int j = 0; j < MAP_RENDER_SIZE; j++) {
                  mapBits[i][j][0] = false;
                  mapBits[i][j][1] = false;
               }
            }
            for (Wizard w : wizards) {
               tx = (int) (((w.x - player.x + SIZE) / MAP_DIVISOR + HALF_MAP_RENDER_SIZE)%(MAP_MOD));
               ty = (int) (((w.y - player.y + SIZE) / MAP_DIVISOR + HALF_MAP_RENDER_SIZE)%(MAP_MOD));
               sx = (w.team == player.team) ? 0 : 1;
               if (tx > 0 && tx < MAP_RENDER_SIZE && ty > 0 && ty < MAP_RENDER_SIZE) {
                  mapBits[tx][ty][sx] = true;
               }
            }
            Graphics g2 = map.getGraphics();
            g2.setColor(Color.GRAY);
            g2.fillRect(1, 1, MAP_RENDER_SIZE, MAP_RENDER_SIZE);
            for (int i = 0; i < MAP_RENDER_SIZE; i++) {
               for (int j = 0; j < MAP_RENDER_SIZE; j++) {
                  if (mapBits[i][j][0] && mapBits[i][j][1]) {
                     g2.setColor(Color.PINK);
                     g2.fillRect(i + 1, j + 1, 1, 1);
                  } else if (mapBits[i][j][0]) {
                     g2.setColor(Color.GREEN);
                     g2.fillRect(i + 1, j + 1, 1, 1);
                  } else if (mapBits[i][j][1]) {
                     g2.setColor(Color.RED);
                     g2.fillRect(i + 1, j + 1, 1, 1);
                  }
               }
            }
            g2.setColor(Color.WHITE);
            g2.fillRect(HALF_MAP_RENDER_SIZE+1, HALF_MAP_RENDER_SIZE+1, 1, 1);
            if (renderMap == 1) {
               g.drawImage(map, 2, 2, null);
            } else if (renderMap == 2) {
               g.drawImage(map, width - (MAP_RENDER_SIZE+4), 2, null);
            }
         }
      }
      if (saveImage || isF7Down) {
         saveImage = false;
         File f = new File("images");
         if (!f.exists() || !f.isDirectory()) {
            f.mkdir();
         }
         f = new File("images"+File.separator+"img" + String.format("%03d", imgcnt) + ".png");
         while (f.exists()) {
            imgcnt++;
            f = new File("images"+File.separator+"img" + String.format("%03d", imgcnt) + ".png");
         }
         try {
            ImageIO.write(image, "PNG", f);
         } catch (IOException ex) {
            ex.printStackTrace();
         }
      }
      return image;
   }

   private static void dropGems(Being b) {
      int tmp;
      for (int i = 1; i < 5; i++) {
         tmp = b.gems[i];
         if (tmp > 490) {
            int cnt = tmp / 49;
            for (int j = 0; j < 49; j++) {
               add(new Gem(i, cnt, b.x, b.y));
            }
            add(new Gem(i, tmp % 49, b.x, b.y));
         } else {
            while (tmp > 0) {
               if (tmp - 10 <= 0) {
                  add(new Gem(i, tmp, b.x, b.y));
               } else {
                  add(new Gem(i, 10, b.x, b.y));
               }
               tmp -= 10;
            }
         }
      }
      while (Math.random() < .4 && !(b instanceof Bird)) {
         add(new Gem("white", (int) (Math.random() * 25) + 25, b.centerX(), b.centerY()));
      }
   }

   public static int getHeight() {
      return height;
   }

   public static int getWidth() {
      return width;
   }

   public static boolean isPlayer(Wizard in) {
      return player == in;
   }

   public static Wizard getPlayer() {
      return player;
   }

   public static double playerDist(double tx, double ty){
      return Math.sqrt(player.distSq(tx, ty));
   }

   public static double playerDist(Entity other){
      return playerDist(other.x, other.y);
   }

   public static double playerXDist(Entity other){
      return player.xDist(other);
   }
   
   public static double playerYDist(Entity other){
      return player.yDist(other);
   }
   
   public static double playerXDist(double x){
      return player.xDist(x);
   }
   
   public static double playerYDist(double y){
      return player.yDist(y);
   }
   
   public static void printWizards(){
      for(Wizard w: wizards){
         System.out.println(w.name+" - ("+w.x+" / "+w.y+")");
      }
   }

   public static void cleanup() {
      if (thinker != null) thinker.stop();
      if (painter != null) painter.stop();
      if (soundManager != null) soundManager.cleanup();
   }
   
//   private static void testAudio(){
//      final float MULT = 1000;
//      final AudioClip ac = AudioClip.get("FullCharge.ogg");
//      ac.forcePlay(false, true);
//      Thread t = new Thread(){
//         @Override
//         public synchronized void run(){
//            int counter = 0;
//            try {
//               System.out.println("Testing +X");
//               while(counter < 30){
//                  wait(DELAY);
//                  ac.setPosition(counter*MULT, 0, 0);
//                  ac.putPosition();
//                  counter++;
//               }
//               counter = 0;
//               System.out.println("Testing -X");
//               while(counter < 30){
//                  wait(DELAY);
//                  ac.setPosition(-counter*MULT, 0, 0);
//                  ac.putPosition();
//                  counter++;
//               }
//               counter = 0;
//               System.out.println("Testing +Y");
//               while(counter < 30){
//                  wait(DELAY);
//                  ac.setPosition(0, counter*MULT, 0);
//                  ac.putPosition();
//                  counter++;
//               }
//               counter = 0;
//               System.out.println("Testing -Y");
//               while(counter < 30){
//                  wait(DELAY);
//                  ac.setPosition(0, -counter*MULT, 0);
//                  ac.putPosition();
//                  counter++;
//               }
//               counter = 0;
//               System.out.println("Testing +Z");
//               while(counter < 30){
//                  wait(DELAY);
//                  ac.setPosition(0, 0, counter*MULT);
//                  ac.putPosition();
//                  counter++;
//               }
//               counter = 0;
//               System.out.println("Testing -Z");
//               while(counter < 30){
//                  wait(DELAY);
//                  ac.setPosition(0, 0, -counter*MULT);
//                  ac.putPosition();
//                  counter++;
//               }
//            } catch (InterruptedException ex) {
//               ex.printStackTrace();
//            }finally{
//               System.out.println("Done Testing Audio");
//               ac.stop();
//            }
//         }
//      };
//      t.start();
//   }
}
