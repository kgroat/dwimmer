/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import java.awt.Rectangle;

/**
 *
 * @author kevingroat
 */
public class BasicMissile extends Missile {
   public static final double MULT = 1.05;

   BasicMissile(double tx, double ty, double tdx, double tdy, int tTeam) {
      particle = new Particle("blue");
      lifespan = 100;
      x = tx-5; y = ty-5+16; z = 32;
      dx = tdx; dy = tdy;
      hitBox = new Rectangle((int)x, (int)y, 9, 9);
      damage = 30;
      team = tTeam;
      sprite = DwimmerEngine.particles.clone();
      sprite.enact("bluel");
      targetable = true;
   }

   @Override
   public void onCollision(Entity other) {
      if(other.team != team){
         final double knockback = .2;
         if(other.isTargetable()){
            this.cleanup();
            if(other instanceof Being){
               ((Being)other).damage(damage);
            }
            other.dx += dx*knockback;
            other.dy += dy*knockback;
         }
      }
   }
   
   @Override
   public void explode() {
      int count = (int)(8+Math.random()*9);
      double vel, theta;
      for(int i=0; i<count; i++){
         vel = Math.random()+.5;
         theta = Math.random()*Math.PI*2;
         DwimmerEngine.add(particle.clone(centerX(), centerY(), z, vel*Math.cos(theta)+dx/5, vel*Math.sin(theta)+dy/5, Math.random()*4-2));
      }
   }
   
}
