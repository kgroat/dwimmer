/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import com.dwimmer.dwimmer.audio.SoundSource;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author kevingroat
 */
public abstract class Entity {
   protected int team;
   protected double x, y, z, dx, dy, dz, fx, fy=-1, ddz, bounciness;
   protected int offsetX, offsetY;
   protected SpriteSet sprite;
   protected Rectangle hitBox;
   protected boolean removeShadow, ignoreCollide;
   private boolean disposable;
   protected enum ColorType { white, red, green, blue, yellow };
   public static boolean drawVector = false;
   public static final double GRAV = -1;
   public boolean targetable;
   protected int targeted;
   protected List<SoundSource> sounds;

   public Entity() {
      this.sounds = new ArrayList<>();
   }

   public void initSounds() {
      if (sounds != null) {
         sounds.forEach(sound -> {
            sound.setPosition((float)-DwimmerEngine.getPlayer().xRel(this), (float)-DwimmerEngine.getPlayer().yRel(this));
            sound.setSpeed((float)this.dx, (float)this.dy);
         });
      }
   }

   public void init() {
      initSounds();
   }

   public void updateSound() {
      if (sounds != null) {
         sounds.forEach(sound -> {
            sound.setPosition((float)-DwimmerEngine.getPlayer().xRel(this), (float)-DwimmerEngine.getPlayer().yRel(this));
            sound.setSpeed((float)this.dx, (float)this.dy);
         });
      }
   }

   public void update(){
      x += dx;
      y += dy;
      z += dz;
      if(dx != 0 || dy != 0){
         fx = dx; fy = dy;
         double div = Math.sqrt(fx*fx + fy*fy);
         fx /= div; fy /= div;
      }
      hitBox.setLocation((int)x+offsetX, (int)y+offsetY);
      if(sprite != null)
         sprite.update();
      physics();
   }
   
   public void paint(Graphics g, Point corner){
      paintShadow(g, corner);
      int iy = corner.y - (int)z/2;
      if(sprite != null){
         sprite.draw(g, corner.x, iy);
      }else{
         if(this instanceof Player)
            g.setColor(Color.yellow);
         else if(this instanceof Wizard)
            g.setColor(Color.green);
         else if(this instanceof Being)
            g.setColor(Color.red);
         else
            g.setColor(Color.cyan);
         g.fillOval(corner.x, iy, hitBox.width, hitBox.height);
         if(this instanceof Player)
            g.setColor(Color.red);
         else if(this instanceof Wizard)
            g.setColor(Color.magenta);
         else if(this instanceof Being)
            g.setColor(Color.white);
         else
            g.setColor(Color.orange);
         g.fillOval(corner.x+hitBox.width/4, iy+hitBox.height/4, hitBox.width/2, hitBox.height/2);
      }
      if(drawVector)
         paintVector(g, corner);
   }
   
   public void paintVector(Graphics g, Point corner){
      if(this instanceof Player)
         g.setColor(Color.magenta);
      else if(this instanceof Wizard)
         g.setColor(Color.white);
      else if(this instanceof Being)
         g.setColor(Color.blue);
      else
         g.setColor(Color.pink);
      double tx = dx*5, ty = this.dy*5, tz = dz*2.5;
      g.fillOval((int)tx+hitBox.width/2-5+corner.x, (int)(ty+tz)+hitBox.height/2-5+corner.y-(int)z/2, 10, 10);
   }
   
   public void paintShadow(Graphics g, Point corner){
      if(!removeShadow){
         Graphics2D g2 = (Graphics2D)g;
         //g2.setColor(Color.magenta);
         g2.setColor(new Color(85, 36, 0, 200));
         int ox, oy, ow, oh;
         ow = Math.max(2, hitBox.width*8/10-(int)z/15);
         oh = ow/2;
         ox = corner.x+offsetX+(hitBox.width - ow)/2;
         oy = corner.y+(int)trueOffset()-oh/2;
         g2.fillOval(ox, oy, ow, oh);
      }
   }
   
   public final boolean collide(Entity other){
      return hitBox.intersects(other.hitBox);
   }
   
   protected void physics(){
      dz += ddz;
      if(z < 0){
         z = 0;
         dz = -dz*bounciness;
         if(dz <= 2) dz = 0;
      }
   }
   
   public abstract void onCollision(Entity other);
   
   public void cleanup(){
      if (this.sounds != null) sounds.forEach(sound -> sound.cleanup());
      disposable = true;
   }
   
   public final boolean isDisposable(){
      return disposable;
   }
   
   public final double centerX(){
      return hitBox.getCenterX();
   }
   
   public final double centerY(){
      return hitBox.getCenterY();
   }
   
   public final double distSq(Entity other){
      return distSq(other.centerX(), other.centerY());
   }
   
   public final double distSq(double tx, double ty){
      double tdx = centerX()-tx, tdy = centerY()-ty;
      tdx = ((tdx + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
      tdy = ((tdy + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
      return tdx*tdx + tdy*tdy;
   }
   
   public final double yDist(Entity other){
      return yDist(other.centerY());
   }
   
   public final double xDist(Entity other){
      return xDist(other.centerX());
   }

   public final double yRel(double y){
      double ty = centerY() - y;
      return ((ty + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
   }
   public final double yRel(Entity other){
      double ty = centerY() - other.centerY();
      return ((ty + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
   }

   public final double xRel(double x){
      double tx = centerX() - x;
      return ((tx + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
   }
   
   public final double xRel(Entity other){
      double tx = centerX() - other.centerX();
      return ((tx + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
   }
   
   public final double yDist(double other){
      double ty = centerY() - other;
      ty = ((ty + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
      return Math.abs(ty);
   }
   
   public final double xDist(double other){
      double tx = centerX() - other;
      tx = ((tx + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
      return Math.abs(tx);
   }
   
   public final double ryDist(Entity other){
      return ryDist(other.y);
   }
   
   public final double rxDist(Entity other){
      return rxDist(other.x);
   }
   
   public final double ryRel(Entity other){
      double ty = y - other.y;
      return ((ty + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
   }
   
   public final double rxRel(Entity other){
      double tx = x - other.x;
      return ((tx + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
   }
   
   public final double ryDist(double other){
      double ty = y - other;
      ty = ((ty + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
      return Math.abs(ty);
   }
   
   public final double rxDist(double other){
      double tx = x - other;
      tx = ((tx + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
      return Math.abs(tx);
   }
   
   public final double xDir(Entity other){
      double tx = other.centerX() - centerX();
      tx = ((tx + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
      return Math.signum(tx);
   }
   
   public final double yDir(Entity other){
      double ty = other.centerY() - centerY();
      ty = ((ty + DwimmerEngine.HALF_SIZE)%DwimmerEngine.SIZE - DwimmerEngine.SIZE)%DwimmerEngine.SIZE + DwimmerEngine.HALF_SIZE;
      return Math.signum(ty);
   }
   
   public final double width(){
      if(sprite == null)
         return hitBox.width;
      else
         return sprite.getSpriteWidth();
   }
   
   public final double height(){
      if(sprite == null)
         return hitBox.height;
      else
         return sprite.getSpriteHeight();
   }
   
   public double trueY(){
      if(sprite != null)
         return y + sprite.getSpriteHeight();
      else
         return y + offsetY + hitBox.height;
   }
   
   public double trueOffset(){
      if(sprite != null)
         return sprite.getSpriteHeight();
      else
         return offsetY + hitBox.height;
   }
   
   public boolean isTargetable(){
      return targetable;
   }
   
   static class EntityComparator implements Comparator<Entity>{

      @Override
      public int compare(Entity one, Entity two) {
         if(one.trueY() < two.trueY())return -1;
         else if(one.trueY() > two.trueY()) return 1;
         else return 0;
      }
      
   }
}
