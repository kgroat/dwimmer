/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

/**
 *
 * @author kevingroat
 */
public class SpinParticle extends Particle{
   
   protected double vTheta, iTheta, diTheta, bdx, bdy, bvel, bdz;
   
   public SpinParticle(String tEffect){
      this(0, 0, 0, 0, 0, 0, tEffect);
   }
   
   public SpinParticle(double tx, double ty, double tz, double tdx, double tdy, double tdz, String tEffect){
      this(tx, ty, tz, tdx, tdy, tdz, tEffect, false, false);
   }
   
   public SpinParticle(double tx, double ty, double tz, double tdx, double tdy, double tdz, String tEffect, boolean tFlip, boolean collide){
      super(tx, ty, tz, tdx, tdy, tdz, tEffect, tFlip, collide);
      bdx = tdx/2;
      bdy = tdy/2;
      bdz = tdz;
      iTheta = 0;
      diTheta = Math.random()*Math.PI/6-Math.PI/12;
      diTheta += Math.signum(diTheta)*Math.PI/12;
      bvel = Math.sqrt(dx*dx + dy*dy)*2;
      vTheta = Math.atan2(dy, dx);
   }
   
   
   public SpinParticle clone(double tx, double ty, double tz, double tdx, double tdy, double tdz){
      return new SpinParticle(tx, ty, tz, tdx, tdy, tdz, effect);
   }
   
   
   public SpinParticle clone(double tx, double ty, double tz, double tdx, double tdy, double tdz, boolean rot, boolean collide){
      return new SpinParticle(tx, ty, tz, tdx, tdy, tdz, effect, rot, collide);
   }
   
   
   @Override
   public void physics(){
      iTheta += diTheta;
      dx = Math.cos(vTheta)*bvel*Math.sin(iTheta)+bdx;
      dy = Math.sin(vTheta)*bvel*Math.sin(iTheta)+bdy;
      dz = bvel*Math.cos(iTheta)+bdz;
   }
   
}
