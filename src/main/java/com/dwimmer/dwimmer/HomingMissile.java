/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import java.awt.Rectangle;

/**
 *
 * @author kevingroat
 */
public class HomingMissile extends Missile {

   private double theta, vel;
   protected Entity target;
   private int countdown;
   private static final double maxTurn = Math.PI/20;

   public HomingMissile(double tx, double ty, int tTeam){
      particle = new Particle("blue");
      x = tx;
      y = ty;
      z = 80;
      dz = 4+Math.random()*4;
      ddz = 0;
      vel = .5+Math.random();
      theta = Math.random()*Math.PI*2;
      dx = Math.cos(theta)*vel;
      dy = Math.sin(theta)*vel;
      team = tTeam;
      target = null;
      lifespan = 100;
      damage = 20;
      countdown = 5+(int)(Math.random()*10);
      hitBox = new Rectangle((int)x, (int)y, 10, 10);
      sprite = DwimmerEngine.particles.clone();
      sprite.enact("bluel");
      targetable = true;
   }
   
   public void update(){
      super.update();
      if(countdown < 0 && (target == null || target.isDisposable())){
         target = DwimmerEngine.closestTarget(this, DwimmerEngine.getWidth()*2/3);
         if(target != null) target.targeted++;
      }
      countdown --;
      ddz = (16-z)/50;
      if(z < 16){
         ddz = -dz*(16-z)/16;
      }
      

      vel = Math.min(10, vel*1.1);
      dx = Math.cos(theta)*vel;
      dy = Math.sin(theta)*vel;
      if(target != null){
         double tdx, tdy, ttheta;
         ttheta = (theta + maxTurn) % (Math.PI*2);
         tdx = Math.cos(ttheta)*vel;
         tdy = Math.sin(ttheta)*vel;
         if(isCloser(tdx, tdy)){
            theta = ttheta;
            dx = tdx;
            dy = tdy;
         }
         ttheta = (theta - maxTurn) % (Math.PI*2);
         tdx = Math.cos(ttheta)*vel;
         tdy = Math.sin(ttheta)*vel;
         if(isCloser(tdx, tdy)){
            theta = ttheta;
            dx = tdx;
            dy = tdy;
         }
      }
   }
   
   private boolean isCloser(double tdx, double tdy){
      return target.distSq(centerX()+dx, centerY()+dy) > target.distSq(centerX()+tdx, centerY()+tdy);
   }
   
   public void physics(){
      dz += ddz;
   }
   
   @Override
   public void onCollision(Entity other) {
      if(other.team != team){
         final double knockback = .2;
         if(other.isTargetable()){
            this.cleanup();
            if(other instanceof Being){
               ((Being)other).damage(damage);
            }
            other.dx += dx*knockback;
            other.dy += dy*knockback;
         }
      }
   }
   
   @Override
   public void explode() {
      //Do nothing ... YET!
   }
   
   public void cleanup(){
      if(!isDisposable() && target != null)
         target.targeted--;
      super.cleanup();
   }
   
}
