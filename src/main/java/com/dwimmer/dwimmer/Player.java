/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

/**
 *
 * @author kevingroat
 */
public class Player extends Wizard {
   
   public Player(){
      this(0, 0, 1);
   }
   
   public Player(double tx, double ty, int tTeam){
      this(tx, ty, tTeam, "Player :D");
   }
   
   public Player(double tx, double ty, int tTeam, String tName){
      super(tx, ty);
      team = tTeam;
      name = tName;
      gems = new int[]{5000, 100, 100, 100, 100};
   }
}
