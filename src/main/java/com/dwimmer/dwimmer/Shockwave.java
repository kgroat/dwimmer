/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import com.dwimmer.dwimmer.audio.SoundSource;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 *
 * @author kevingroat
 */
public class Shockwave {
   
   double x, y, radius, maxRad, odx, ody;
   int team, power, curr;
   int total;
   private boolean disposable;
   private static int counter = 0;

   private SoundSource sound;
   
   public Shockwave(double tx, double ty, int tPower, int tTeam){
      x = tx; y = ty;
      power = tPower;
      team = tTeam;
      curr = 0;
      total = 15;
      radius = power/10.+30;
      maxRad = power*1.05+30;
      
      counter%=3;
      counter++;

      sound = new SoundSource(false, true);
      sound.setBuffer(RandomUtils.chooseRandom(DwimmerEngine.blasts));
      sound.setPosition((float)-DwimmerEngine.getPlayer().xRel(x), (float)-DwimmerEngine.getPlayer().yRel(y));
      sound.play();

//      AudioClip ac = AudioClip.get("Blast_"+counter+".ogg");
//      float[] vals = {(float)DwimmerEngine.playerXDist(x), (float)DwimmerEngine.playerYDist(y), 0};
//      if(ac.isPertinent(vals)){
//         ac.setPosition(vals);
//               ac.setGainFromDist(DwimmerEngine.playerDist(x, y));
//         ac.forcePlay(false, true);
//      }
   }
   
   public void update(){
      radius += power/(curr+1)/5;
      if(curr >= total)
         cleanup();
      curr++;
   }
   
   public void collideAndEffect(Entity other){
      double dist = dist(other);
      if(other.team != team && radius>dist){
         double val = power*(maxRad-dist)/maxRad;
         findDir(other, dist);
         other.dx+=odx*val/15;
         other.dy+=ody*val/15;
         if(other instanceof Being){
            ((Being)other).damage((int)(val/2));
         }else{
            if(Math.random()<.2) other.cleanup();
            if(other instanceof HomingMissile || other instanceof BasicMissile){
               other.dx = odx*val/15;
               other.dy = ody*val/15;
            }
         }
      }
   }
   
   public void render(BufferedImage bi, int cx, int cy){
      //For now:
      RippleEffect.operate(bi, cx, cy, radius, (total-curr)*(total-curr)*(power+20.)/120/20/15, Math.PI/2, 0);//curr/2.5);
      Graphics g = bi.getGraphics();
      g.setColor(new Color(255, 200, 0, 50*(total-curr)/total));
      g.fillOval((int)(cx-radius), (int)(cy-radius), (int)(2*radius), (int)(2*radius));
   }
   
   public void findDir(Entity other, double dist){
      odx = (other.centerX()-x)/dist;
      ody = (other.centerY()-y)/dist;
      
   }
   
   public double dist(Entity other){
      double dx = other.centerX()-x, dy = other.centerY()-y;
      return Math.sqrt(dx*dx+dy*dy);
   }
   
   public boolean isDisposable(){
      return disposable;
   }
   
   public void cleanup(){
      sound.cleanup();
      disposable = true;
   }
   
}
