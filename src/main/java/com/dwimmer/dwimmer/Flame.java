/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

import com.dwimmer.dwimmer.audio.SoundSource;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

/**
 *
 * @author kevingroat
 */
public class Flame extends Missile{

   public static final int START_LIFE = 20;
   public static final double FAST = START_LIFE-2;
   public static final double MEDIUM = START_LIFE-5;
   public static final double SLOW = START_LIFE-10;
   
   public static double DRAG = .90;
   public static final int pEvery = 5;
   
   private double theta, dtheta, baseDamage;
   
   private Particle particle2, particle3;
   
   public Flame(double tx, double ty, double tdx, double tdy, int tTeam) {
      particle = new FireParticle();
      particle2 = new SpinParticle("red");
      particle3 = new SpinParticle("red2");
      lifespan = START_LIFE;
      x = tx-5; y = ty-5+16; z = 32;
      dx = tdx; dy = tdy;
      hitBox = new Rectangle((int)x, (int)y, 9, 9);
      baseDamage = 8;
      team = tTeam;
      sprite = DwimmerEngine.fire.clone();
      sprite.enact("boom");
      removeShadow = true;
      if(Math.random() < .7){
         ignoreCollide = true;
      }
      theta = Math.random()*Math.PI*2;
      dtheta = (Math.signum(Math.random()-.5))*(.3+Math.random()*.1);
      z = 32;
      ddz = -GRAV/10;
      
      if(Math.random()<.2){
         SoundSource sound = new SoundSource(false, true);
         sound.setBuffer(RandomUtils.chooseRandom(DwimmerEngine.fires));
         sound.play();
         sounds.add(sound);
      }
   }

   public void update(){
      physics();
      x += dx;
      y += dy;
      z += dz;
      if(dx != 0 || dy != 0){
         fx = dx; fy = dy;
         double div = Math.sqrt(fx*fx + fy*fy);
         fx /= div; fy /= div;
      }
      hitBox.setLocation((int)x+offsetX, (int)y+offsetY);
      updateSound();
      
      if(lifespan > FAST) sprite.setCurrentFrame(0);
      else if(lifespan > MEDIUM) sprite.setCurrentFrame(1);
      else if(lifespan > SLOW) sprite.setCurrentFrame(2);
      else sprite.setCurrentFrame(3);
      
      theta += dtheta;
      if(particle != null && (Math.random()*pEvery)<1 && lifespan > SLOW){
         DwimmerEngine.add(particle.clone(x+(Math.random()*hitBox.width)-particle.width()/2,
                                          y+(Math.random()*hitBox.height)-particle.width()/2,
                                          z, dx/10, dy/10, 1, true, false));
      }
      damage = (int)(baseDamage*lifespan/START_LIFE);
      lifespan--;
      if(lifespan <= 0)
         cleanup();
   }
   
   public void paint(Graphics g, Point corner){
      int iy = corner.y - (int)z/2;
      sprite.drawRot(g, corner.x, iy, theta);
      if(drawVector)
         paintVector(g, corner);
   }
   
   public void physics(){
      dx *= DRAG;
      dy *= DRAG;
      dz += ddz;
   }
   
   @Override
   public void explode() {
      if(Math.random()<.5){
         double vel, theta;
         vel = Math.random()+.5;
         theta = Math.random()*Math.PI*2;
         DwimmerEngine.add(particle3.clone(centerX(), centerY(), z, vel*Math.cos(theta), vel*Math.sin(theta), Math.random()*4-2));
      }
   }
   
   @Override
   public void onCollision(Entity other){
      final double knockback = .05;
      if(other.isTargetable()){
         if(other.team != team){
            this.explode();
            other.dx += dx*knockback;
            other.dy += dy*knockback;
            if(other instanceof Being){
               ((Being)other).damage(damage);
            }
         }
      }
   }

   
}
