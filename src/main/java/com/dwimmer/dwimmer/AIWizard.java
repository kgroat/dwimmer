/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dwimmer.dwimmer;

/**
 *
 * @author kevingroat
 */
public class AIWizard extends Player{
   
   protected Entity target;
   protected Gem gem;
   protected int stepsLeft;
   protected int bVert, bHoriz;
   public static final int minSteps = 50;
   public static final int stepVar = 450;
   public static final double viewDist = 300;
   public static final double[] dists = {50, 10, 100, 150, 40};
   public static final double[] attackDists = {0, 100, 1000, 200, 50};
   public static final double fireStop = 0;
   
   public AIWizard(double tx, double ty, int tTeam){
      super(tx, ty, tTeam, "AIWizard D:");
      team = tTeam;
      x = tx; y = ty;
      gems = new int[]{500, 100, 100, 100, 100};
   }
   
   public void update(){
      super.update();
      if(!DwimmerEngine.isPlayer(this)){
         switch(type){
            case white: aiWhite(); break;
            case red: aiRed(); break;
            case green: aiGreen(); break;
            case blue: aiBlue(); break;
            case yellow: aiYellow(); break;
         }
      }
   }

   public boolean nearGems(){
      gem = DwimmerEngine.closestGem(this, viewDist);
      return gem != null;
   }
   
   public void aiWhite(){
      W = A = S = D = false;
      SP = false;
      if(target != null && target.isDisposable())
         target = null;
      Entity tEnt = null;
      //if(Math.random()<.1)
         tEnt = DwimmerEngine.weightedTarget(this, viewDist, 80);
      if(tEnt != null && tEnt != target)
         target = tEnt;
      //System.out.println("Target: " + target);
      if(target != null && !(target instanceof Missile)){
         if(hasGoodGems()){
            if(gems[0] < 100){
               //<editor-fold defaultstate="collapsed" desc="Running low on health">
               if(I){
                  //If charging missile swarm
                  if((gems[3] == 0 || charge > Math.max(gems[0], minGems[3])) && Math.sqrt(distSq(target)) < attackDists[3]){
                     I=false;
                     DI=true;
                  }
               }else if(J){
                  //If shooting fire
                  if((gems[1] == 0 || Math.random()<fireStop) || Math.sqrt(distSq(target)) > attackDists[1]){
                     J=false;
                  }
               }else if(K){
                  //If charging shockwave
                  if((gems[4] == 0 || charge > Math.max(gems[0], minGems[4]*2)) && Math.sqrt(distSq(target)) < attackDists[4]){
                     K=false;
                     DK=true;
                  }
               }else if(L){
                  //If making birds
                  if(gems[2] == 0){
                     L=false;
                     DL=true;
                  }
               }else{
                  //If not casting
                  if(gems[1] > 10 && Math.sqrt(distSq(target))<attackDists[1]){
                     J=true;
                  }else if(hasGoodGems()){
                     int tmp=0;
                     boolean done = false;
                     if(distSq(target) < attackDists[1]){
                        int count = 0;
                        do{
                           count++;
                           if(count % 1000 == 0){
                              System.out.println("OOPS1: " + count);
                           }
                           tmp = (int)(Math.random()*4)+1;
                           switch(tmp){
                              case 1: done = gems[tmp]>minGems[tmp] && distSq(target) < attackDists[1]; break;
                              case 2: done = gems[tmp]>minGems[tmp]; break;
                              case 3: done = gems[tmp]>minGems[tmp]; break;
                              case 4: done = gems[tmp]>minGems[tmp]; break;
                           }
                        }while(!done);
                     }else if(hasGoodGems234()){
                        int count = 0;
                        do{
                           count++;
                           if(count % 1000 == 0){
                              System.out.println("OOPS2: " + count);
                           }
                           tmp = (int)(Math.random()*3)+2;
                           switch(tmp){
                              case 2: done = gems[tmp]>minGems[tmp]; break;
                              case 3: done = gems[tmp]>minGems[tmp]; break;
                              case 4: done = gems[tmp]>minGems[tmp]; break;
                           }
                        }while(!done);
                     }
                     switch(tmp){
                        case 1: J = true; break;
                        case 2: L = true; break;
                        case 3: I = true; break;
                        case 4: K = true; break;
                     }
                  }
               }
               //</editor-fold>
            }else{
               if(distSq(target)>9900){
                  //<editor-fold defaultstate="collapsed" desc="Target is not very close">
                  if(I){
                     if((gems[3] == 0 || charge == 100) && Math.sqrt(distSq(target)) < attackDists[3]){
                        I=false;
                        DI=true;
                     }
                  }else if(J){
                     if((gems[1] == 0 || Math.random()<fireStop) || Math.sqrt(distSq(target)) > attackDists[1]){
                        J=false;
                     }
                  }else if(K){
                     if((gems[4] == 0 || charge == 100) && Math.sqrt(distSq(target)) < attackDists[4]){
                        K=false;
                        DK=true;
                     }
                  }else if(L){
                     if(gems[2] == 0){
                        L=false;
                        DL=true;
                     }
                  }else{
                     if(hasGoodGems()){
                        int tmp=0;
                        boolean done = false;
                        if(distSq(target) < attackDists[1]){
                           int count = 0;
                           do{
                              count++;
                              if(count % 1000 == 0){
                                 System.out.println("OOPS3: " + count);
                              }
                              tmp = (int)(Math.random()*4)+1;
                              switch(tmp){
                                 case 1: done = gems[tmp]>minGems[tmp] && distSq(target) < attackDists[1]; break;
                                 case 2: done = gems[tmp]>minGems[tmp]; break;
                                 case 3: done = gems[tmp]>minGems[tmp]; break;
                                 case 4: done = gems[tmp]>minGems[tmp]; break;
                              }
                           }while(!done);
                        }else if(hasGoodGems234()){
                           do{
                              tmp = (int)(Math.random()*3)+2;
                              switch(tmp){
                                 case 2: done = gems[tmp]>minGems[tmp]; break;
                                 case 3: done = gems[tmp]>minGems[tmp]; break;
                                 case 4: done = gems[tmp]>minGems[tmp]; break;
                              }
                           }while(!done);
                        }
                        switch(tmp){
                           case 1: J = true; break;
                           case 2: L = true; break;
                           case 3: I = true; break;
                           case 4: K = true; break;
                        }
                     }
                  }
                  //</editor-fold>
               }else{
                  //<editor-fold defaultstate="collapsed" desc="Target is too close for comfort">
                  if(I){
                     if((gems[3] == 0 || charge > Math.max(Math.sqrt(distSq(target)), minGems[3])) && Math.sqrt(distSq(target)) < attackDists[3]){
                        I=false;
                        DI=true;
                     }
                  }else if(J){
                     if((gems[1] == 0 || Math.random()<fireStop) || Math.sqrt(distSq(target)) > attackDists[1]){
                        J=false;
                     }
                  }else if(K){
                     if((gems[4] == 0 || charge > Math.max(Math.sqrt(distSq(target)), minGems[4]*2)) && Math.sqrt(distSq(target)) < attackDists[4]){
                        K=false;
                        DK=true;
                     }
                  }else if(L){
                     L=false;
                     DL=true;
                  }else{
                     if(gems[1]>0){
                        J = true;
                     }else if(hasGoodGems134()){
                        int tmp=0;
                        boolean done = false;
                        if(distSq(target) < attackDists[1]){
                           int count = 0;
                           do{
                              count++;
                              if(count % 1000 == 0){
                                 System.out.println("OOPS4: " + count);
                              }
                              tmp = (int)(Math.random()*4)+1;
                              switch(tmp){
                                 case 1: done = gems[tmp]>minGems[tmp] && distSq(target) < attackDists[1]; break;
                                 case 3: done = gems[tmp]>minGems[tmp]; break;
                                 case 4: done = gems[tmp]>minGems[tmp]; break;
                              }
                           }while(!done);
                        }else if(hasGoodGems34()){
                           do{
                              tmp = (int)(Math.random()*3)+2;
                              switch(tmp){
                                 case 3: done = gems[tmp]>minGems[tmp]; break;
                                 case 4: done = gems[tmp]>minGems[tmp]; break;
                              }
                           }while(!done);
                        }
                        switch(tmp){
                           case 1: J = true; break;
                           case 3: I = true; break;
                           case 4: K = true; break;
                        }
                     }
                  }
                  //</editor-fold>
               }
            }
         }else if(nearGems()){
            moveToGem();
         }else{
            SP = true;
         }
      }else if(target != null){
         //Target is a missile
         if(hasGoodGems()){
            if(gems[0] < 100){
               //<editor-fold defaultstate="collapsed" desc="Running low on health">
               if(I){
                  //If charging missile swarm
                  if((gems[3] == 0 || charge > Math.max(gems[0], minGems[3])) && Math.sqrt(distSq(target)) < attackDists[3]){
                     I=false;
                     DI=true;
                  }
               }else if(J){
                  J = false;
               }else if(K){
                  //If charging shockwave
                  if((gems[4] == 0 || charge > Math.max(gems[0], minGems[4]*2)) && Math.sqrt(distSq(target)) < attackDists[4]){
                     K=false;
                     DK=true;
                  }
               }else if(L){
                  //If making birds
                  if(gems[2] == 0){
                     L=false;
                     DL=true;
                  }
               }else{
                  //If not casting
                  if(gems[1] > 10 && Math.sqrt(distSq(target))<attackDists[1]){
                     J=true;
                  }else if(hasGoodGems234()){
                     int tmp=0;
                     boolean done = false;
                     int count = 0;
                     do{
                        count++;
                        if(count % 1000 == 0){
                           System.out.println("OOPS2: " + count);
                        }
                        tmp = (int)(Math.random()*3)+2;
                        switch(tmp){
                           case 2: done = gems[tmp]>minGems[tmp]; break;
                           case 3: done = gems[tmp]>minGems[tmp]; break;
                           case 4: done = gems[tmp]>minGems[tmp]; break;
                        }
                     }while(!done);
                     switch(tmp){
                        case 2: L = true; break;
                        case 3: I = true; break;
                        case 4: K = true; break;
                     }
                  }
               }
               //</editor-fold>
            }else{
               if(distSq(target)>9900){
                  //<editor-fold defaultstate="collapsed" desc="Target is not very close">
                  if(I){
                     if((gems[3] == 0 || charge == 100) && Math.sqrt(distSq(target)) < attackDists[3]){
                        I=false;
                        DI=true;
                     }
                  }else if(J){
                     J=false;
                  }else if(K){
                     if((gems[4] == 0 || charge == 100) && Math.sqrt(distSq(target)) < attackDists[4]){
                        K=false;
                        DK=true;
                     }
                  }else if(L){
                     if(gems[2] == 0){
                        L=false;
                        DL=true;
                     }
                  }else{
                     if(hasGoodGems234()){
                        int tmp=0;
                        boolean done = false;
                        int count = 0;
                        do{
                           tmp = (int)(Math.random()*3)+2;
                           switch(tmp){
                              case 2: done = gems[tmp]>minGems[tmp]; break;
                              case 3: done = gems[tmp]>minGems[tmp]; break;
                              case 4: done = gems[tmp]>minGems[tmp]; break;
                           }
                        }while(!done);
                        switch(tmp){
                           case 2: L = true; break;
                           case 3: I = true; break;
                           case 4: K = true; break;
                        }
                     }
                  }
                  //</editor-fold>
               }else{
                  //<editor-fold defaultstate="collapsed" desc="Target is too close for comfort">
                  if(I){
                     if((gems[3] == 0 || charge > Math.max(Math.sqrt(distSq(target)), minGems[3])) && Math.sqrt(distSq(target)) < attackDists[3]){
                        I=false;
                        DI=true;
                     }
                  }else if(J){
                     J=false;
                  }else if(K){
                     if((gems[4] == 0 || charge > Math.max(Math.sqrt(distSq(target)), minGems[4]*2)) && Math.sqrt(distSq(target)) < attackDists[4]){
                        K=false;
                        DK=true;
                     }
                  }else if(L){
                     L=false;
                     DL=true;
                  }else{
                     if(hasGoodGems34()){
                        int tmp=0;
                        boolean done = false;
                        int count = 0;
                        do{
                           tmp = (int)(Math.random()*3)+2;
                           switch(tmp){
                              case 3: done = gems[tmp]>minGems[tmp]; break;
                              case 4: done = gems[tmp]>minGems[tmp]; break;
                           }
                        }while(!done);
                        switch(tmp){
                           case 3: I = true; break;
                           case 4: K = true; break;
                        }
                     }
                  }
                  //</editor-fold>
               }
            }
         }else if(nearGems()){
            moveToGem();
         }else{
            SP = true;
         }
      }else{
         //<editor-fold defaultstate="collapsed" desc="No target chosen">
         //Charge random spell
         if(!(I||J||K||L)){
            L = gems[2] > minGems[2];
            if(!L && hasGoodGems234()){
               int tmp;
               boolean done = false;
               int count = 0;
               do{
                  count++;
                  if(count % 1000 == 0){
                     System.out.println("OOPS5: " + count);
                  }
                  tmp = (int)(Math.random()*2)+3;
                  switch(tmp){
                     case 3: done = gems[tmp]>minGems[tmp]; break;
                     case 4: done = gems[tmp]>minGems[tmp]; break;
                  }
               }while(!done);
               switch(tmp){
                  case 3: I = true; break;
                  case 4: K = true; break;
               }
            }
         }else if(L && gems[2]+charge < minGems[2]){
            L = false;
         }else if(J){
            J = false;
         }
         //</editor-fold>
      }
      if(target != null && (hasGoodGems() || !nearGems()) && gems[0]>100){
         moveToTarget();
      }else if(gems[0]<=100){
         runAwayFromTarget();
      }else{
         lookForGems();
      }
      //System.out.println("Inputs: "+I+" / "+J+" / "+K+" / "+L+" / "+DI+" / "+DK+" / "+DL);
   }
   
   public void aiRed(){
      
   }
   
   public void aiGreen(){
      
   }
   
   public void aiBlue(){
      
   }
   
   public void aiYellow(){
      
   }
   
   public void lookForGems(){
      if(gem != null && !gem.isDisposable()){
         moveToGem();
      }else{
         if(stepsLeft > 0){
            keepMoving();
         }else{
            chooseRandomPath();
         }
         //Mull about
      }
   }
   
   protected void keepMoving(){
      stepsLeft--;
      if(bVert == -2 || (bVert == -1 && stepsLeft%2 == 0)){
         W = true;
      }else if(bVert == 2 || (bVert == 1 && stepsLeft%2 == 0)){
         S = true;
      }
      
      if(bHoriz == -2 || (bHoriz == -1 && stepsLeft%2 == 0)){
         A = true;
      }else if(bHoriz == 2 || (bHoriz == 1 && stepsLeft%2 == 0)){
         D = true;
      }
      
   }
   
   protected void chooseRandomPath(){
      stepsLeft = minSteps + (int)(Math.random()*stepVar);
      bVert = (int)(Math.random()*5)-2;
      bHoriz = (int)(Math.random()*5)-2;
   }
   
   public void moveToGem(){
      double dist = dists[type.ordinal()];
      double tdx1=dx, tdy1=dy, tdx2, tdy2;
      boolean tW=false, tA=false, tS=false, tD=false;
      for(int i=-1; i<2; i++){
         for(int j=-1; j<2; j++){
            tdx2 = (i==0?dx*DRAG:dx+i*ACCELERATION);
            tdy2 = (j==0?dy*DRAG:dy+j*ACCELERATION);
            if(isGemCloser(tdx1, tdy1, tdx2, tdy2)){
               tdx1 = tdx2;
               tdy1 = tdy2;
               tA = i<0;
               tD = i>0;
               tW = j<0;
               tS = j>0;
            }
         }
      }
      A = tA; D = tD; W = tW; S = tS;
   }
   
   public void moveToTarget(){
      if(target != null){
         double dist = dists[type.ordinal()];
         double tdx1=dx, tdy1=dy, tdx2, tdy2;
         boolean tW=false, tA=false, tS=false, tD=false;
         for(int i=-1; i<2; i++){
            for(int j=-1; j<2; j++){
               tdx2 = (i==0?dx*DRAG:dx+i*ACCELERATION);
               tdy2 = (j==0?dy*DRAG:dy+j*ACCELERATION);
               if(isCloser(tdx1, tdy1, tdx2, tdy2)){
                  tdx1 = tdx2;
                  tdy1 = tdy2;
                  tA = i<0;
                  tD = i>0;
                  tW = j<0;
                  tS = j>0;
               }
            }
         }
         A = tA; D = tD; W = tW; S = tS;
         lookAtTarget();
      }
   }
   
   public void runAwayFromTarget(){
      if(target != null){
         double dist = dists[type.ordinal()];
         double tdx1=dx, tdy1=dy, tdx2, tdy2;
         boolean tW=false, tA=false, tS=false, tD=false;
         for(int i=-1; i<2; i++){
            for(int j=-1; j<2; j++){
               tdx2 = (i==0?dx*DRAG:dx*DRAG+i*ACCELERATION);
               tdy2 = (j==0?dy*DRAG:dy*DRAG+j*ACCELERATION);
               if(isCloserRunaway(tdx1, tdy1, tdx2, tdy2)){
                  tdx1 = tdx2;
                  tdy1 = tdy2;
                  tA = i<0;
                  tD = i>0;
                  tW = j<0;
                  tS = j>0;
               }
            }
         }
         A = tA; D = tD; W = tW; S = tS;
         lookAtTarget();
      }
   }
   
   private void lookAtTarget(){
      double tdx = xRel(target);
      double tdy = yRel(target);
      double dist = Math.sqrt(distSq(target));
      fx = -tdx/dist;
      fy = -tdy/dist;
   }
   
   private boolean isCloser(double tdx1, double tdy1, double tdx2, double tdy2){
      double dist = dists[type.ordinal()];
      return Math.abs(target.distSq(centerX()+tdx1, centerY()+tdy1)-dist*dist) > 
             Math.abs(target.distSq(centerX()+tdx2, centerY()+tdy2)-dist*dist);
   }
   
   private boolean isCloserRunaway(double tdx1, double tdy1, double tdx2, double tdy2){
      double dist = Math.max(10, dists[type.ordinal()]*2-gems[0]);
      return Math.abs(target.distSq(centerX()+tdx1, centerY()+tdy1)-dist*dist) > 
             Math.abs(target.distSq(centerX()+tdx2, centerY()+tdy2)-dist*dist);
   }
   
   private boolean isGemCloser(double tdx1, double tdy1, double tdx2, double tdy2){
      return gem.distSq(centerX()+tdx1, centerY()+tdy1) > 
             gem.distSq(centerX()+tdx2, centerY()+tdy2);
   }
   
}
