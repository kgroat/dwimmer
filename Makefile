
all: build-windows-32 build-windows-64 build-macos-intel

build-windows-32:
	mvn package -P lwjgl-natives-windows-x86
	mkdir -p jars
	mv target/Maven_Dwimmer-1.0-SNAPSHOT-jar-with-dependencies.jar jars/Dwimmer_win32.jar

build-windows-64:
	mvn package -P lwjgl-natives-windows-amd64
	mkdir -p jars
	mv target/Maven_Dwimmer-1.0-SNAPSHOT-jar-with-dependencies.jar jars/Dwimmer_win64.jar

build-macos-intel:
	mvn package -P lwjgl-natives-macos-amd64
	mkdir -p jars
	mv target/Maven_Dwimmer-1.0-SNAPSHOT-jar-with-dependencies.jar jars/Dwimmer_macos_intel.jar
